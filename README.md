## Introduction

Gateware and iOS app necessary to control second radio channel of LimeSDR via the first radio channel operating as Bluetooth LE peripheral. Both channels will be locked at 2.4 GHz due to the routing of the PLLs. Gateware and iOS app will be hosted in different directories. Development journal in journal directory.

## Proposal

The bulk of the changes to the gateware will be localized to the rxtx_top module. Illustration is below:

![alt_text](drafted_design.png "DRAFT")

Channel B will operate as closely to the normal gateware as possible. Channel A will not have a direct path to the PHY as it does in the default gateware. As the LimeSDR is operating as a GATT Server and GAP Peripheral, the bulk of TX, aside from the advertising event, will be in respone to a request from the iPhone. The roles of the GAP peripheral are listed in Table 2-1 in Vol 3, Part C of the Bluetooth 5.0 core specification.

The GATT sever will have two sets of attributes, both stored on the external DRAM which is normally used for WFM storage in the default LimeSDR gateware. One will be for storing data to be sent over the Bluetooth LE link to the IPhone upon request. The other will be for storing data from the IPhone configuring the LMS7002m parameters for Channel B TX.

My intial work on this project will be the tedious and complex task of implementing the Bluetooth "firmware-level" components in VHDL. At that point, I will be able to evaluate whether the design is feasible on the LimeSDR's Cyclone IV. The most recent LimeSDR firmware uses 29203 out of 39600 logic gates and 832160 out of 1161216 memory bits. The Bluetooth LE Controller, L2CAP Handler, and GATT server will thus together have to use 10000 logic gates.