## Part One: Setup VHDL compiler for Cyclone IV on macOS

Unfortunately, running Quartus Prime Lite (which only supports Linux and Windows) is an absolute necessity to create the .rbf file that the USB controller on the LimeSDR-USB then takes the flash the Cyclone IV.

Why create gateware on Mac at all? It is convenient in my opinion for this entire project to be done on a single computer, and iOS development is best done natively on Mac in my experience.

The approach I have taken at least initially is to use Ubuntu through VirtualBox to run Quartus. This is a fairly straightforward process (though it does take a while to download and install everything), with only one hiccup that I encountered...

I wanted to mount a file from my host filesystem to the virtual machine, but the Shared Folder option in the VirtualBox settings for my virtual machine was having no effect. The solution was first to issue the following command inside the Ubuntu image to grant priveleges to the virtual machine: 

```
sudo apt-get install virtualbox-guest-x11
```

Next, this command is entered:
```
sudo usermod -a -G vboxsf [username]
```
Credit goes to theDrake’s comment on this thread: 
https://stackoverflow.com/questions/23514244/share-folders-from-the-host-mac-os-to-a-guest-linux-system-in-virtualbox

After these two settings, the mounted folder will be accessible inside the virtual machine within the /media/ directory at the root of the filesystem.

Another tip, the default virtualbox display is quite small. You can adjust it within the Display menu in Ubuntu. It is accessible via command line through:
```
gnome-control-center display
```

After it is all said and done, quartus runs, although quite slowly. It took 15 minutes to compile the project from scratch. This may need to be improved with a command line emulation approach (quartus does provide command line tools), though the virtual box approach will allow for use of the simulation functionality in the Quartus GUI. 

![](quartus.png)


